# grafana-stack-for-raspberry
This repo was created to deploy a full *observability stack* for **Raspberry Pi OS (64-bit)** using *docker-compose*.

Follow this useful [guidance](https://dev.to/elalemanyo/how-to-install-docker-and-docker-compose-on-raspberry-pi-1mo) to install docker and docker-compose on *Raspberry pi*.


## Running docker-compose

```bash
# from root directory run
docker-compose -f docker-compose.yaml up -d
# in case you don't have root access
sudo docker-compose -f docker-compose.yaml up -d

```


## Accessing to grafana
There two ways to access Grafana,

- from the host that is running the containers [](http://localhost:3000/)
- or from a different host in the same network [](http://[raspberry_pi_IP]:3000/)

## Plug and play grafana datasources and dashboard
The Grafana datasources to prometheus and loki are already configured so you can directly go to discovery section and see the data and also a pre-configured dashboard for Linux metrics.

![Alt text](linux-dashboard.png)

## References
- [Monitoring a Linux host with Prometheus, Node Exporter, and Docker Compose](https://grafana.com/docs/grafana-cloud/quickstart/docker-compose-linux/)

- [Gathering logs from a Linux host using the Grafana Agent](https://grafana.com/docs/grafana-cloud/quickstart/logs_agent_linuxnode/)



